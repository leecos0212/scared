VERSION = (0, 9, 1)

__version__ = ".".join(map(str, VERSION))
__copyright__ = "(c) 2015-2022 ESHARD"
